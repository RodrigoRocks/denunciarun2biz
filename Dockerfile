# Imagem base do mavem com java 17 para compilação.
FROM maven:3.8-openjdk-17-slim AS build
# Diretorio da aplicação
WORKDIR /app
# Copiando para pasta de trabalho
COPY . /app
# Compilar o projeto
RUN mvn clean package

# Imagem base  java 17 para execução.
FROM openjdk:17-slim
# Deixar a porta 8080 exposta
EXPOSE 8080
# Diretorio base da aplicação
WORKDIR /app
# Copiar do conteiner de compilação para conteiner de execução
COPY --from=build /app/target/appDenuncia.jar /appDenuncia.jar

#Rodar aplicação
ENTRYPOINT ["java","-jar","/appDenuncia.jar"]