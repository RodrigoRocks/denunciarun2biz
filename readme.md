# Teste Backend Java

Autor: Rodrigo Carlos Cardoso
Data: 13/02/2023

### Finalidade:
Seu desafio será desenvolver um serviço de denúncias e, como parte dele, veremos como você estrutura as camadas de aplicação, chamadas externas, variáveis de ambiente, cache, testes unitários, logs e documentação.

### Funcionalidades:
✓ Incluir Denuncia (http://localhost:8080/v1/denuncias);
	
Json:

```

			{
    			"latitude": -15.789925709252136,
			    "longitude": -47.887251273393815,
			    "denunciante": {
			      "nome": "Rodrigo ",
			      "cpf": "45616532145"
			    },
			    "denuncia": {
			      "titulo": "Esgoto a céu aberto",
			      "descricao": "Existe um esgoto a céu aberto nesta localidade."
			    }
			}
			
```

### Check list:

✓ Que o desafio seja feito em Java;

✓ Que considere utilizar cache para consultas ao serviço de geolocalização;

✓ TDD;
 
✓ Princípios SOLID;

✓ 12Factor;

✓ Passo-a-passo de como rodar sua aplicação;

Conteinerização da aplicação utilizando docker e docker compose.

### Pré requisitos

1. Java 17
2. Maven/Gradle
3. JPA
4. Spring Boot
5. Spring Data
6. H2database
7. IDE
8. Postman


### Instalando o back-end

1. Importar os projetos para uma IDE (Eclipse| Intellij | Visual Studio Code Com extensões para Java)
2. Iniciar os projetos na seguinte ordem:
  
```
mvn clean install

```
3. Executar DenunciaApplication.java como Spring Boot App.

## Documentações de referência para bibliotecas utilizadas

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.3.5.RELEASE/maven-plugin/reference/html/)
* [Spring Boot DevTools](https://docs.spring.io/spring-boot/docs/2.3.5.RELEASE/reference/htmlsingle/#using-boot-devtools)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.3.5.RELEASE/reference/htmlsingle/#boot-features-developing-web-applications)
* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [H2database](http://www.h2database.com/html/features.html)

## Acessar H2

URL de acesso:
* [http://localhost:8080/h2-console/](http://www.h2database.com/html/features.html)

Dados de conexão estão no arquivo application.yml


## Validações

1. Request inválido. [400]
2. Endereço não encontrado para essa localidade. [404]

##Docker
Criar Docker

```
docker build -t denuncia:latest .
```

Executar Docker compose

```
docker-compose up -d
```