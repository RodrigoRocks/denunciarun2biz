package br.com.denuncia.controller;


import java.math.BigDecimal;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.denuncia.dto.DenunciaDTO;
import br.com.denuncia.dto.DenuncianteDTO;
import br.com.denuncia.dto.MotivoDTO;
import br.com.denuncia.model.Denuncia;
import br.com.denuncia.repository.DenunciaRepository;

/**
 * @author Rodrigo Carlos Cardoso - 20/01/2022 08:29:26
 *
 */

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class DenunciaControllerTest {
	
	@Autowired
	private TestRestTemplate restTemplate;
	
	@MockBean
	private DenunciaRepository denunciaRepository; 
	
	@Test
	public void postDadosNaoInformadosStatus400 () {
		BDDMockito.when(denunciaRepository.save(criarDenuncia())).thenReturn(criarDenuncia());
		ResponseEntity<String> response = restTemplate.postForEntity("/v1/denuncias", DenunciaDTO.builder().build(), String.class);
		Assertions.assertThat(response.getStatusCodeValue()).isEqualTo(400);
		Assertions.assertThat(response.getBody()).contains("mensagem","Request inválido.");
	}
	
	
	@Test
	public void postDadosNaoInformadosCamposStatus400 () {
		BDDMockito.when(denunciaRepository.save(criarDenuncia())).thenReturn(criarDenuncia());
		ResponseEntity<String> response = restTemplate.postForEntity("/v1/denuncias", criarDenunciaErroCampos(), String.class);
		Assertions.assertThat(response.getStatusCodeValue()).isEqualTo(400);
		Assertions.assertThat(response.getBody()).contains("mensagem","Request inválido.");
	}
	
	@Test
	public void postLocalidaNaoEncontrdaStatus400 () {
//		BDDMockito.when(denunciaRepository.save(criarDenuncia())).thenThrow(null);
		ResponseEntity<String> response = restTemplate.postForEntity("/v1/denuncias", criarDenunciaErroLocalidade(), String.class);
		Assertions.assertThat(response.getStatusCodeValue()).isEqualTo(404);
		Assertions.assertThat(response.getBody()).contains("mensagem","Endereço não encontrado para essa localidade.");
	}
	
	@Test
	public void postStatus200 () {
		BDDMockito.when(denunciaRepository.save(criarDenuncia())).thenReturn(criarDenuncia());
		ResponseEntity<String> response = restTemplate.postForEntity("/v1/denuncias", criarDenunciaDTO(), String.class);
		Assertions.assertThat(response.getStatusCodeValue()).isEqualTo(201);
	}
	
	private Denuncia criarDenuncia() {
		Denuncia denuncia = Denuncia.builder().build();
		return denuncia;
	}
	
	
	private DenunciaDTO criarDenunciaErroLocalidade() {
		DenunciaDTO denunciaDTO = DenunciaDTO.builder().latitude(BigDecimal.valueOf(999l)).longitude(BigDecimal.valueOf(999l))
				.motivo(new MotivoDTO()).denunciante(new DenuncianteDTO()).build();
		
		denunciaDTO.getDenunciante().setNome("Teste");
		denunciaDTO.getDenunciante().setCpf("00000000000");
		
		denunciaDTO.getMotivo().setDescricao("Teste descricao");
		denunciaDTO.getMotivo().setTitulo("Teste Titulo");
		return denunciaDTO;
	}

	private DenunciaDTO criarDenunciaErroCampos() {
		DenunciaDTO denunciaDTO = DenunciaDTO.builder().latitude(BigDecimal.ZERO).longitude(BigDecimal.ZERO)
				.denunciante(new DenuncianteDTO()).build();
		
		denunciaDTO.getDenunciante().setNome(null);
		denunciaDTO.getDenunciante().setCpf(null);
		
		return denunciaDTO;
	}
	
	private DenunciaDTO criarDenunciaDTO() {
		DenunciaDTO denunciaDTO = DenunciaDTO.builder().latitude(BigDecimal.ZERO).longitude(BigDecimal.ZERO)
				.motivo(new MotivoDTO()).denunciante(new DenuncianteDTO()).build();
		return denunciaDTO;
	}
	

	
}
