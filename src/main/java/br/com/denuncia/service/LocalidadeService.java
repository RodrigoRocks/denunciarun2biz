package br.com.denuncia.service;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.denuncia.dto.EnderecoDTO;
import br.com.denuncia.helper.LocalidadeServiceHelper;
import lombok.extern.log4j.Log4j2;

@Service
@Transactional	
@Log4j2
public class LocalidadeService {
	@Value("${localidade.url}")
	private String url;	
		
	@Value("${localidade.key}")
	private String key;

		
	public EnderecoDTO fazerRequisicaoREST(BigDecimal latitude, BigDecimal longitude) {
		
		log.info("Inciar pesquisar localidade", latitude, longitude);
		StringBuilder urlCompleta = new StringBuilder(url); 
		urlCompleta.append("?key="+key+"&location="+latitude+","+longitude+"&includeRoadMetadata=true&includeNearestIntersection=true");
		log.info("URL conexao: {}", urlCompleta);
		String errorMessage = "Endereço não encontrodo.";
		return (EnderecoDTO)LocalidadeServiceHelper.doGetRequisicao(urlCompleta.toString(), EnderecoDTO.class,  errorMessage , "");
	}


}

	
