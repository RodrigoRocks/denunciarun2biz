package br.com.denuncia.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.denuncia.dto.DenunciaDTO;
import br.com.denuncia.dto.EnderecoDTO;
import br.com.denuncia.error.ResourceLocalidadeException;
import br.com.denuncia.model.Denuncia;
import br.com.denuncia.model.Denunciante;
import br.com.denuncia.model.Endereco;
import br.com.denuncia.model.Motivo;
import br.com.denuncia.repository.DenunciaRepository;

@Service
public class DenunciaService {
	
	private static final Logger LOG = LoggerFactory.getLogger(DenunciaService.class);
	
	@Autowired
	private DenunciaRepository repository;
	
	@Autowired
	private LocalidadeService localidadeService; 
	
	@Transactional
	public Denuncia salvarDenuncia(DenunciaDTO denunciaDTO) {

		LOG.info("Inclusão da denuncia, Localidade: {} - {}", denunciaDTO.getLatitude(), denunciaDTO.getLongitude());
		
		EnderecoDTO dto = localidadeService.fazerRequisicaoREST(denunciaDTO.getLatitude(), denunciaDTO.getLongitude());
		
		localidadeNaoEcontrada(dto);
		
		Denuncia denuncia = criarDenuncia();
		BeanUtils.copyProperties(denunciaDTO, denuncia);
		BeanUtils.copyProperties(denunciaDTO.getDenunciante(), denuncia.getDenunciante());
		BeanUtils.copyProperties(denunciaDTO.getMotivo(), denuncia.getDenuncia());
		denuncia.setEndereco(new Endereco(dto));
		
		
		denuncia = salvar(denuncia);

		LOG.info("Salvando da denuncia, Localidade: {} - {}", denunciaDTO.getLatitude(), denunciaDTO.getLongitude());
		
		return denuncia;
	}

	private Denuncia salvar(Denuncia denuncia) {
		return repository.save(denuncia);
	}

	private void localidadeNaoEcontrada(EnderecoDTO dto) {
		if(dto == null) {
			throw new ResourceLocalidadeException("002", "Endereço não encontrado para essa localidade.");
		}
	}

	private Denuncia criarDenuncia() {
		Denuncia denuncia = Denuncia.builder().denunciante(new Denunciante()).denuncia(new Motivo()).build();
		return denuncia;
	}

}
