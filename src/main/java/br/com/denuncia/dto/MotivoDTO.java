package br.com.denuncia.dto;

import javax.validation.constraints.NotEmpty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MotivoDTO {
	

	@NotEmpty(message = "Campo obrigatório não informado")
	private String titulo;
	
	@NotEmpty(message = "Campo obrigatório não informado")
	private String descricao;

	@Override
	public String toString() {
		return "DenunciaDTO [titulo=" + titulo + ", descricao=" + descricao + "]";
	}
	
}
