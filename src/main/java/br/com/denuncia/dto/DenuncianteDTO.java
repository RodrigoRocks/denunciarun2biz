package br.com.denuncia.dto;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DenuncianteDTO {
	
	@NotNull(message = "Request inválido.")
	private String nome;
	
	@NotNull(message = "Request inválido.")
	private String cpf;

	@Override
	public String toString() {
		return "DenuncianteDTO [nome=" + nome + ", CPF=" + cpf + "]";
	}
	
}
