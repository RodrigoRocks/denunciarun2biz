package br.com.denuncia.dto;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonAlias;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class DenunciaDTO {
	
	@NotNull(message = "Request inválido.")
	private BigDecimal latitude;
	
	@NotNull(message = "Request inválido.")
	private BigDecimal longitude;
	
	@NotNull(message = "Request inválido.")
	private DenuncianteDTO denunciante;
	
	@NotNull(message = "Request inválido.")
	@JsonAlias({"denuncia","motivo"})
	private MotivoDTO motivo;

	@Override
	public String toString() {
		return "DenunciaRegistroDTO [latitude=" + latitude + ", longitude=" + longitude
				+ ", denunciante=" + denunciante + ", denuncia=" + motivo + "]";
	}
	
}
