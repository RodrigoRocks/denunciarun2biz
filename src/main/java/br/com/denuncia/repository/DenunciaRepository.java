package br.com.denuncia.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.denuncia.model.Denuncia;

@Repository
public interface DenunciaRepository  extends JpaRepository<Denuncia, Long>{
	
}
