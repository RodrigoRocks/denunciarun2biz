package br.com.denuncia.handler;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import br.com.denuncia.error.ErrorDetails;
import br.com.denuncia.error.ResourceConflictDetails;
import br.com.denuncia.error.ResourceForbiddenDetails;
import br.com.denuncia.error.ResourceForbiddenException;
import br.com.denuncia.error.ResourceLocalidadeException;
import br.com.denuncia.error.ValidationErrorDetails;

@ControllerAdvice
public class RestExceptionHandler {
	
	@ExceptionHandler(ResourceForbiddenException.class)
	public ResponseEntity<?> handleResourceForbiddenException(ResourceForbiddenException rfException){
		ResourceForbiddenDetails rfDetails = ResourceForbiddenDetails.builder()
		.mensagem(rfException.getMessage()).build();
		
		return new ResponseEntity<>(rfDetails, HttpStatus.FORBIDDEN);
		
	}
	
	@ExceptionHandler(ConstraintViolationException.class)
	public ResponseEntity<?> handleConstraintViolationException(ConstraintViolationException rcException){
		ResourceConflictDetails rcDetails = ResourceConflictDetails.builder()
				.error(ErrorDetails.builder()
		.mensagem("Request inválido.")
		.codigoErro("001").build()).build();
		return new ResponseEntity<>(rcDetails, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(ResourceLocalidadeException.class)
	public ResponseEntity<?> handleResourceLocalidadeException(ResourceLocalidadeException rcException){
		ResourceConflictDetails rcDetails = ResourceConflictDetails.builder()
				.error(ErrorDetails.builder()
		.mensagem(rcException.getMessage())
		.codigoErro(rcException.getCodigoErro()).build()).build();
		
		return new ResponseEntity<>(rcDetails, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<?> handleMethodArgumentNotValidException(MethodArgumentNotValidException manvException){
		
		List<FieldError> listaErro = manvException.getBindingResult().getFieldErrors();
		listaErro.stream().map(fr -> {
			return fr.getDefaultMessage();
		}).collect(Collectors.joining(","));
		ValidationErrorDetails rfDetails = ValidationErrorDetails.builder()
		.mensagem(listaErro.stream().map(fr -> {
			return fr.getDefaultMessage();
		}).collect(Collectors.joining(",")))
		.codigoErro("001")
		.build();
		return new ResponseEntity<>(rfDetails, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(HttpMessageNotReadableException.class)
	public ResponseEntity<?> handleHttpMessageNotReadableException(HttpMessageNotReadableException manvException){
		
		ResourceConflictDetails rcDetails = ResourceConflictDetails.builder()
				.error(ErrorDetails.builder()
		.mensagem("Request inválido.")
		.codigoErro("001").build()).build();
		
		return new ResponseEntity<>(rcDetails, HttpStatus.BAD_REQUEST);
	}

}



