package br.com.denuncia.error;

import lombok.Getter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
public class ResourceConflictDetails {
	private ErrorDetails error; 
	
}
