package br.com.denuncia.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import lombok.Getter;

@ResponseStatus(code = HttpStatus.CONFLICT)
@Getter
public class ResourceLocalidadeException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	private String codigoErro;
	
	public ResourceLocalidadeException(String codigoErro, String message) {
		super(message);
		this.codigoErro = codigoErro;
	}

}
