package br.com.denuncia.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;


/**
 * @author Rodrigo Carlos Cardoso 13/02/2023
 *
 */
@Entity
@Table(name = "DENUNCIANTE")
@Getter
@Setter
public class Denunciante extends AbstractEntity{
	
	private static final long serialVersionUID = -3195521163941271484L;

	@Column
	@NotNull(message = "Request inválido.")
	private String nome;
	
	@Column
	@NotNull(message = "Request inválido.")
	private String cpf;

	@Override
	public String toString() {
		return "Denunciante [nome=" + nome + ", cpf=" + cpf + "]";
	}
   
}
