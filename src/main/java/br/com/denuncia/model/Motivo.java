package br.com.denuncia.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

import lombok.Getter;
import lombok.Setter;


/**
 * @author Rodrigo Carlos Cardoso 13/02/2023
 *
 */
@Entity
@Table(name = "MOTIVO")
@Getter
@Setter
public class Motivo extends AbstractEntity{
	
	private static final long serialVersionUID = -3195521163941271484L;

	@Column
	@NotEmpty(message = "Request inválido.")
	private String titulo;
	
	@Column
	@NotEmpty(message = "Request inválido.")
	private String descricao;

	@Override
	public String toString() {
		return "DenunciaDTO [titulo=" + titulo + ", descricao=" + descricao + "]";
	}
	
   
}
