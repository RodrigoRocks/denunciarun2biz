package br.com.denuncia.model;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;


/**
 * @author Rodrigo Carlos Cardoso 13/02/2023
 *
 */
@Entity
@Table(name = "DENUNCIA")
@Builder
@Getter
@Setter
public class Denuncia extends AbstractEntity{
	
	private static final long serialVersionUID = -3195521163941271484L;

	@Column
	private BigDecimal latitude;
	
	@Column
	private BigDecimal longitude;
	
	@ManyToOne(cascade=CascadeType.ALL)
	private Motivo denuncia;
		
	@ManyToOne(cascade=CascadeType.ALL)
	private Denunciante denunciante;
	
	@ManyToOne(cascade=CascadeType.ALL)
	private Endereco endereco;

	@Override
	public String toString() {
		return "Denuncia [latitude=" + latitude + ", longitude=" + longitude + ", motivo=" + denuncia + ", denunciante="
				+ denunciante + ", endereco=" + endereco + "]";
	}
	
	
}
