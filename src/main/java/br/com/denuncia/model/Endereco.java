package br.com.denuncia.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import br.com.denuncia.dto.EnderecoDTO;
import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "DENUNCIA")
@Getter
@Setter
public class Endereco extends AbstractEntity{
	

	private static final long serialVersionUID = 4339247024175282949L;
	@Column
	private String logradouro;
	@Column
	private String bairro;
	@Column
	private String cidade;
	@Column
	private String estado;
	@Column
	private String pais;
	@Column
	private String cep;
	@Override
	public String toString() {
		return "Endereco [logradouro=" + logradouro + ", bairro=" + bairro + ", cidade=" + cidade + ", estado=" + estado
				+ ", pais=" + pais + ", cep=" + cep + "]";
	}
	public Endereco(EnderecoDTO dto) {
		super();
		this.logradouro = dto.getStreet();
		
		this.bairro = dto.getAdminArea6();
		
		this.cidade = dto.getAdminArea5();
		
		this.estado = dto.getAdminArea3();
		
		this.pais = dto.getAdminArea1();
		
		this.cep = dto.getPostalCode();
		
	}
	
	
	
}
