package br.com.denuncia.helper;


import java.io.IOException;
import java.net.ConnectException;

import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;

import br.com.denuncia.dto.ResultadoDTO;
import br.com.denuncia.error.ResourceLocalidadeException;
import br.com.denuncia.error.ResourceNotFoundException;
import br.com.denuncia.util.rest.ClientREST;
import br.com.denuncia.util.rest.RespostaREST;

public class LocalidadeServiceHelper {

	private LocalidadeServiceHelper() {
	}

	public static ResourceLocalidadeException criarException(RespostaREST respostaREST, String urlAcessada) {
		Gson gson = new Gson();
		ResultadoDTO resultadoDTO = gson.fromJson(respostaREST.getCorpo(), ResultadoDTO.class);
		if (StringUtils.isNotEmpty(resultadoDTO.getMensagem())) {
			resultadoDTO.setMensagem(respostaREST.getMensagem());
		}
		String message = String.format("Codigo retorno: %s, Mensagem: %s, URL: %s", 
					respostaREST.getCodigo(), 
					StringUtils.isNotEmpty(resultadoDTO.getMensagem()) ? resultadoDTO.getMensagem() : "Erro sem mensagem.", 
					urlAcessada);
		return new ResourceLocalidadeException("001", message);
	} 
	
	public static Object doGetRequisicao(String urlCompleta, Class<?> nomeClasse, String errorMessage, String mensagemComplementoException) throws ResourceNotFoundException {
		try {
			RespostaREST resultado = ClientREST.getObjetoSemAutenticacao(urlCompleta, "");
			Gson gson = new Gson();
			if (resultado.isSucesso()) {
				try {
					Object dto = gson.fromJson(resultado.getEndereco(), nomeClasse);
					return dto;
				}catch (IndexOutOfBoundsException e) {
					return null;
				}
				
			} else {
				throw criarException(resultado, urlCompleta);
			}
		} catch (ConnectException e) {
			throw criarConnectException(mensagemComplementoException, e, urlCompleta);
		} catch (IOException e) {
			throw criarIOException(mensagemComplementoException, e, urlCompleta);
		}
	}

	private static ResourceLocalidadeException criarIOException(String mensagemComplementoException, IOException e, String urlCompleta) {
		return new ResourceLocalidadeException("001",
				mensagemComplementoException + " Erro ao chamar o serviço de localidade");
	}

	private static ResourceLocalidadeException criarConnectException(String mensagemComplementoException, ConnectException e, String urlCompleta) {
		return new ResourceLocalidadeException("001", mensagemComplementoException + " Erro de conexão com o Localidade");
	}


}
