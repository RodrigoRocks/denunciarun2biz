package br.com.denuncia.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.denuncia.dto.DenunciaDTO;
import br.com.denuncia.dto.RespostaDTO;
import br.com.denuncia.service.DenunciaService;

@RestController
@RequestMapping("v1/denuncias")
public class DenunciaController {
	
	@Autowired
	private DenunciaService denunciaService; 

	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RespostaDTO> marcarHora(@Valid @RequestBody(required = true) DenunciaDTO denunciaRegistroDTO) {
		return new ResponseEntity<RespostaDTO>(new RespostaDTO(denunciaService.salvarDenuncia(denunciaRegistroDTO)),HttpStatus.CREATED);
	}

}
