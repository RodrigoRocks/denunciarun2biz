package br.com.denuncia.util.rest;

import java.io.IOException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;

import com.google.common.base.Preconditions;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * @author dalembert.cruz
 */
public final class ClientREST<T> {

	public static final MediaType JSON = MediaType.get("application/json; charset=utf-8");
	public static String CHARSET = "UTF-8";

	/**
	 * Método responsável por realizar uma conexão Rest usando sem autenticação.
	 * Padrão: REST Tipo de Operação: GET
	 *
	 * @param urlWebService - url de acesso ao rest
	 * @param key       - Chave de acesso ao da API.
	 * @param parametros    - parametros necessários para acessar o rest.
	 * @return Json
	 * @throws IOException
	 * @author rodrigo.carlos
	 * @data 13/02/2023
	 */
	public static RespostaREST getObjetoSemAutenticacao(String urlWebService, String key, String... parametros)
			throws IOException {
		
		validarArgumentosObrigatorios(urlWebService);

		urlWebService = verificarParametros(urlWebService, parametros);

		CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
		OkHttpClient httpClient = new OkHttpClient();

		Request request = new Request.Builder().url(urlWebService)
				.addHeader("Content-Type", "application/json;charset=" + CHARSET)
				.addHeader("Cache-Control", "public, max-age=1000").build();

		return tratarResposta(httpClient, request);
	}

	private static void validarArgumentosObrigatorios(String urlWebService) {
		Preconditions.checkArgument(urlWebService != null, "A url é obrigatória.");
	}

	private static RespostaREST tratarResposta(OkHttpClient httpClient, Request request) throws IOException {
		RespostaREST respostaREST = new RespostaREST();
		
		try (Response response = httpClient.newCall(request).execute()) {
			respostaREST.setCodigo(response.code());
			String cabecalho = response.headers().toMultimap().toString();
			respostaREST.setCabecalho(cabecalho);
			respostaREST.setSucesso(response.isSuccessful());
			respostaREST.setMensagem(response.message());

			//trata os erros 400 (erros do cliente)
			if (response.code() > 399 && response.code() < 500) {
				respostaREST.setCorpo(String.format("{ \"mensagem\":\"%s\"}", response.message()));
			} else {
				ResponseBody body = response.body();
				respostaREST.setCorpo(body.string());
			}
		}
		
		return respostaREST;
	}

	private static String verificarParametros(String urlWebService, String... parametros) {
		if (parametros != null)
			for (String parametro : parametros) {
				urlWebService = urlWebService + "/" + parametro.replaceAll(" ", "%20");
			}
		return urlWebService;
	}

}
