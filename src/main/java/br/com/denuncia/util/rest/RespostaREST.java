package br.com.denuncia.util.rest;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RespostaREST {
	
	private int codigo;
	private boolean sucesso;
	private String cabecalho;
	private String mensagem;
	private String corpo;
	
	
	public String getEndereco () {
		JsonObject jsonObject = new JsonParser().parse(corpo).getAsJsonObject();
		return jsonObject.get("results").getAsJsonArray().get(0).getAsJsonObject().get("locations").getAsJsonArray().get(0).getAsJsonObject().toString();
	}
	

}