package br.com.denuncia.util;

import java.util.Calendar;

public class DataUtil {
	
	/**
     * Verifica se data á sábado ou domingo e acrescenta dias conforme necessário p/ retornar dia de semana.
     *
     * @param   data        Um objeto Calendar
     * @return  Calendar
     */
    public static Boolean checarFDS(Calendar data)
    {
        // Se for sabado ou domingo 
        if (data.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY || data.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY){
            return true;
        }
        return false;
    }

}
